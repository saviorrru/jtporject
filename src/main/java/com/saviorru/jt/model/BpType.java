package com.saviorru.jt.model;

public enum BpType {
    LEGAL("LEGAL"),
    INDIVIDUAL("INDIVIDUAL");

    BpType(String name) {
        this.name = name;
    }
    public final String name;
}
