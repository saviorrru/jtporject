package com.saviorru.jt.model;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name="tms")
public class TM {

    @Id
    private String objectId;
    private String objectName;
    private Float averageV;
    private Float cost;
    private String kn;
    private Float power;
    private String f;
    private String askue;
    private String model;
    private String no;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doc_id")
    private SupplyDocument document;

    public TM() {
    }

    public TM(String objectId, String objectName, Float averageV, Float cost, String kn, Float power, String f, String askue, String model, String no, SupplyDocument document) {
        this.objectId = objectId;
        this.objectName = objectName;
        this.averageV = averageV;
        this.cost = cost;
        this.kn = kn;
        this.power = power;
        this.f = f;
        this.askue = askue;
        this.model = model;
        this.no = no;
        this.document = document;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }


    public Float getAverageV() {
        return averageV;
    }

    public void setAverageV(Float averageV) {
        this.averageV = averageV;
    }

    public Float getCost() {
        return cost;
    }

    public void setCost(Float cost) {
        this.cost = cost;
    }

    public String getKn() {
        return kn;
    }

    public void setKn(String kn) {
        this.kn = kn;
    }

    public Float getPower() {
        return power;
    }

    public void setPower(Float power) {
        this.power = power;
    }

    public String getF() {
        return f;
    }

    public void setF(String f) {
        this.f = f;
    }

    public String getAskue() {
        return askue;
    }

    public void setAskue(String askue) {
        this.askue = askue;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public SupplyDocument getDocument() {
        return document;
    }

    public void setDocument(SupplyDocument document) {
        this.document = document;
    }
}
