package com.saviorru.jt.model;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class TMIdentity implements Serializable {
    @NotNull
    private String bu;
    @NotNull
    private String plant;
    @NotNull
    private String substr;
    @NotNull
    private String TM;

    public TMIdentity() {
    }

    public TMIdentity(@NotNull String bu, @NotNull String plant, @NotNull String substr, @NotNull String TM) {
        this.bu = bu;
        this.plant = plant;
        this.substr = substr;
        this.TM = TM;
    }

    public String getBu() {
        return bu;
    }

    public void setBu(String bu) {
        this.bu = bu;
    }

    public String getPlant() {
        return plant;
    }

    public void setPlant(String plant) {
        this.plant = plant;
    }

    public String getSubstr() {
        return substr;
    }

    public void setSubstr(String substr) {
        this.substr = substr;
    }

    public String getTM() {
        return TM;
    }

    public void setTM(String TM) {
        this.TM = TM;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TMIdentity that = (TMIdentity) o;
        return bu.equals(that.bu) &&
                plant.equals(that.plant) &&
                substr.equals(that.substr) &&
                TM.equals(that.TM);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bu, plant, substr, TM);
    }
}
