package com.saviorru.jt.model;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="supply_docs")
public class SupplyDocument {
    @Id
    private String regNo;
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Temporal(TemporalType.DATE)
    private Date endDate;

    public BuisinessPartner getBp() {
        return bp;
    }

    public void setBp(BuisinessPartner bp) {
        this.bp = bp;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bp_id")
    private BuisinessPartner bp;

    public SupplyDocument() {
    }

    public SupplyDocument(String regNo, Date startDate, Date endDate) {
        this.regNo = regNo;
        this.startDate = startDate;
        this.endDate = endDate;
    }


    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
