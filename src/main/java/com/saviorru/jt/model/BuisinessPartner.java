package com.saviorru.jt.model;
import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name="bps")
public class BuisinessPartner {
    @Id
    private String Id;


    private String bpType;
    private String bpRole;
    private String bpName;


    public BuisinessPartner() {
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getBpType() {
        return bpType;
    }

    public void setBpType(String bpType) {
        this.bpType = bpType;
    }

    public String getBpRole() {
        return bpRole;
    }

    public void setBpRole(String bpRole) {
        this.bpRole = bpRole;
    }

    public String getBpName() {
        return bpName;
    }

    public void setBpName(String bpName) {
        this.bpName = bpName;
    }
}
