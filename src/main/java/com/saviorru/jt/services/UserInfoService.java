package com.saviorru.jt.services;

import com.saviorru.jt.model.UserInfo;

import java.util.List;

public interface UserInfoService {
    void save(UserInfo userInfo);
    void update(UserInfo userInfo);
    void delete(UserInfo userInfo);
    UserInfo findById(Long id);
    List<UserInfo> findAll();


}
