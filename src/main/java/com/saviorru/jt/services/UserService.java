package com.saviorru.jt.services;

import com.saviorru.jt.model.User;
import org.springframework.data.jpa.repository.Modifying;

import java.util.List;

public interface UserService {
    void save(User user);
    void update(User user);
    void delete(User user);
    void updatePassword(User user);
    User findByUsername(String username);
    List<User> getAll();
}
