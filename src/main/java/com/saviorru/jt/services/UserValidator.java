package com.saviorru.jt.services;

import com.saviorru.jt.controllers.support.RawUserInfo;
import com.saviorru.jt.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

@Component
public class UserValidator implements Validator {

    private final UserService userService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserValidator(UserService userService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userService = userService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;

        checkValidString(errors, "username", user.getUsername(), 1, 32, "Size.userForm.username");

        if (userService.findByUsername(user.getUsername()) != null) {
            errors.rejectValue("username", "Duplicate.userForm.username");
        }

        checkValidString(errors, "password", user.getPassword(), 4, 32, "Size.userForm.password");

    }


    public void validatePassword(Object o, Errors errors) {
        User user = (User) o;
        checkValidString(errors, "password", user.getPassword(), 4, 32, "Size.userForm.password");
    }

    public void validateUserInfo(Object o, Errors errors)
    {
        RawUserInfo rawUserInfo = (RawUserInfo) o;
        if (rawUserInfo.getEmail()!= "") {
            String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                    "[a-zA-Z0-9_+&*-]+)*@" +
                    "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                    "A-Z]{2,7}$";

            Pattern pat = Pattern.compile(emailRegex);
            if (!pat.matcher(rawUserInfo.getEmail()).matches()) {
                errors.rejectValue("email", "Account.wrongUserInfo.email");
            }
        }
        if (rawUserInfo.getBirthDate()!="") {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            sdf.setLenient(false);

            try {
                Date date = sdf.parse(rawUserInfo.getBirthDate());
                System.out.println(date);

            } catch (ParseException e) {
                errors.rejectValue("birthDate", "Account.wrongUserInfo.birthDate");
            }
        }
    }



    protected void checkValidString(Errors errors, String field, String value, int minLength, int maxLength, String s) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, field, "NotEmpty");
        if (value.length() < minLength || value.length() > maxLength) {
            errors.rejectValue(field, s);
        }
    }
}
