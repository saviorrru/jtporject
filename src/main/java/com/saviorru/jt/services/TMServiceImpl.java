package com.saviorru.jt.services;

import com.saviorru.jt.model.TM;
import com.saviorru.jt.model.TMIdentity;
import com.saviorru.jt.repository.TMRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TMServiceImpl implements TMService {
    private final TMRepository tmRepository;

    @Autowired
    public TMServiceImpl(TMRepository tmRepository) {
        this.tmRepository = tmRepository;
    }


    @Override
    public void save(TM tm) {
        this.tmRepository.saveAndFlush(tm);

    }

    @Override
    public void update(TM tm) {
        Optional<TM> existingTM = this.tmRepository.findById(tm.getObjectId());
        if (existingTM.isPresent())
        {
            tmRepository.saveAndFlush(tm);
        }
    }

    @Override
    public void delete(TM tm) {
        this.tmRepository.delete(tm);

    }

    @Override
    public TM findById(String id) {
        Optional<TM> existingTM = this.tmRepository.findById(id);
        return existingTM.orElse(null);
    }

    @Override
    public List<TM> getAll() {
        return this.tmRepository.findAll();
    }
}
