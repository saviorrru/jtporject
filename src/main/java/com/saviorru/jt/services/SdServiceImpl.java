package com.saviorru.jt.services;

import com.saviorru.jt.model.SupplyDocument;
import com.saviorru.jt.repository.SupplyDocRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SdServiceImpl implements SdService {
    private final SupplyDocRepository supplyDocRepository;


    public SdServiceImpl(SupplyDocRepository supplyDocRepository) {
        this.supplyDocRepository = supplyDocRepository;
    }


    @Override
    public void save(SupplyDocument doc) {
        this.supplyDocRepository.saveAndFlush(doc);

    }

    @Override
    public void update(SupplyDocument doc) {
        Optional<SupplyDocument> existingSd = supplyDocRepository.findById(doc.getRegNo());
        if (existingSd.isPresent())
        {
            this.supplyDocRepository.saveAndFlush(doc);
        }

    }

    @Override
    public void delete(SupplyDocument doc) {
        this.supplyDocRepository.delete(doc);
    }

    @Override
    public SupplyDocument findById(String id) {
        Optional<SupplyDocument> existingSd = supplyDocRepository.findById(id);
        return existingSd.orElse(null);
    }

    @Override
    public List<SupplyDocument> getAll() {
        return this.supplyDocRepository.findAll();
    }
}
