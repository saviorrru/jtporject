package com.saviorru.jt.services;


import com.saviorru.jt.model.Role;
import com.saviorru.jt.model.User;

public interface RoleService {
    Role findRoleByName(String name);
    void createRole(String name);
    void deleteRole(String name);
    void addRoleToUser(Role role, User user);
    void removeRoleFromUser(Role role, User user);
}