package com.saviorru.jt.services;

import com.saviorru.jt.model.UserInfo;
import com.saviorru.jt.repository.UserInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserInfoServiceImpl implements UserInfoService {
    private final UserInfoRepository userInfoRepository;

    @Autowired
    public UserInfoServiceImpl(UserInfoRepository userInfoRepository) {
        this.userInfoRepository = userInfoRepository;
    }

    @Override
    public void save(UserInfo userInfo) {
        userInfoRepository.save(userInfo);
    }

    @Override
    public void update(UserInfo userInfo) {
        Optional<UserInfo> existingUserInfo = userInfoRepository.findById(userInfo.getId());
        if (existingUserInfo.isPresent())
        {
            userInfoRepository.saveAndFlush(userInfo);
        }
    }

    @Override
    public void delete(UserInfo userInfo) {
        userInfoRepository.delete(userInfo);
    }

    @Override
    public UserInfo findById(Long id) {
        return userInfoRepository.findById(id).orElse(null);
    }

    @Override
    public List<UserInfo> findAll() {
        return userInfoRepository.findAll();
    }
}
