package com.saviorru.jt.services;


import com.saviorru.jt.model.BuisinessPartner;

import java.util.List;

public interface BpService {
    void save(BuisinessPartner bp);
    void update(BuisinessPartner bp);
    void delete(BuisinessPartner bp);
    BuisinessPartner findById(String id);
    List<BuisinessPartner> getAll();
}
