package com.saviorru.jt.services;

import com.saviorru.jt.model.SupplyDocument;

import java.util.List;

public interface SdService {
    void save(SupplyDocument doc);
    void update(SupplyDocument doc);
    void delete(SupplyDocument doc);
    SupplyDocument findById(String id);
    List<SupplyDocument> getAll();
}
