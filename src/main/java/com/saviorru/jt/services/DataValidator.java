package com.saviorru.jt.services;

import com.saviorru.jt.controllers.support.RawSdInfo;
import com.saviorru.jt.controllers.support.RawTMInfo;
import com.saviorru.jt.model.BuisinessPartner;
import com.saviorru.jt.model.SupplyDocument;
import com.saviorru.jt.model.TM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Component
public class DataValidator {

    private final BpService bpService;
    private final SdService sdService;
    private  final TMService tmService;

    @Autowired
    public DataValidator(BpService bpService, SdService sdService, TMService tmService) {

        this.bpService = bpService;
        this.sdService = sdService;
        this.tmService = tmService;
    }

    public void validateBp(Object o, Errors errors)
    {
        BuisinessPartner bp = (BuisinessPartner) o;
        checkValidString(errors, "id", bp.getId(), 1, 20, "Data.bp.invalid.length.id");
        validateBpId(o, errors);

    }

    public void validateBpId(Object o, Errors errors)
    {
        BuisinessPartner bp = (BuisinessPartner) o;
        if (bpService.findById(bp.getId()) != null) {
            errors.rejectValue("id", "Data.invalid.duplicate");
        }

    }

    public void validateSd(Object o, Errors errors)
    {
        RawSdInfo sd = (RawSdInfo) o;
        checkValidString(errors, "regNo", sd.getRegNo(), 1, 40, "Data.sd.invalid.length.id");
        if (!isDateVaild(sd.getStartDate()))
        {
            errors.rejectValue("startDate", "Account.wrongUserInfo.birthDate");
        }
        if (!isDateVaild(sd.getEndDate()))
        {
            errors.rejectValue("endDate", "Account.wrongUserInfo.birthDate");
        }
        if((sd.getBp()!="") && (bpService.findById(sd.getBp()) == null))
        {
            errors.rejectValue("bp", "Data.invalid.refId");
        }
    }


    public void validateSdId(Object o, Errors errors)
    {
        RawSdInfo sd = (RawSdInfo) o;
        if (sdService.findById(sd.getRegNo()) != null) {
            errors.rejectValue("regNo", "Data.invalid.duplicate");
        }
    }

    protected boolean isDateVaild(String dateStr)
    {
        if (dateStr !="") {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            sdf.setLenient(false);

            try {
                Date date = sdf.parse(dateStr);
                System.out.println(date);

            } catch (ParseException e) {
                return false;
            }
        }
        return true;
    }

    public  void validateTMId(Object o, Errors errors)
    {
        RawTMInfo tm = (RawTMInfo) o;
        if (tmService.findById(tm.getObjectId()) != null) {
            errors.rejectValue("objectId", "Data.invalid.duplicate");
        }
    }

    public void validateTM(Object o, Errors errors)
    {
        RawTMInfo tm = (RawTMInfo) o;
        checkValidString(errors, "objectId", tm.getObjectId(), 1, 25, "Data.tm.invalid.length.id");
        if (!checkFloat(tm.getAverageV()) && tm.getAverageV()!="" )
        {
            errors.rejectValue("averageV", "Data.tm.invalid.float");
        }
        if (!checkFloat(tm.getPower()) && tm.getPower()!="")
        {
            errors.rejectValue("power", "Data.tm.invalid.float");
        }
        if (!checkFloat(tm.getCost()) && tm.getCost()!="")
        {
            errors.rejectValue("cost", "Data.tm.invalid.float");
        }
        if((tm.getDocument()!="") && (sdService.findById(tm.getDocument()) == null))
        {
            errors.rejectValue("document", "Data.invalid.refId");
        }


    }

    protected boolean checkFloat(String fStr)
    {
        try {
            double d = Float.parseFloat(fStr);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }


    protected void checkValidString(Errors errors, String field, String value, int minLength, int maxLength, String s) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, field, "NotEmpty");
        if (value.length() < minLength || value.length() > maxLength) {
            errors.rejectValue(field, s);
        }
    }

}
