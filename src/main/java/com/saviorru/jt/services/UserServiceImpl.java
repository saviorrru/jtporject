package com.saviorru.jt.services;

import com.saviorru.jt.model.RoleName;
import com.saviorru.jt.model.User;
import com.saviorru.jt.model.UserInfo;
import com.saviorru.jt.repository.RoleRepository;
import com.saviorru.jt.repository.UserInfoRepository;
import com.saviorru.jt.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserInfoRepository userInfoRepository;
    private final RoleRepository roleRepository;
    private final RoleService roleService;


    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, RoleService roleService, UserInfoRepository userInfoRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.roleService = roleService;
        this.userInfoRepository = userInfoRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;

        for (RoleName roleName : RoleName.values()) {
            roleService.createRole(roleName.name);
        }
    }

    @Override
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRoles(new HashSet<>(Collections.singleton(roleRepository.findByName(RoleName.USER.name))));
        userRepository.saveAndFlush(user);
    }

    @Override
    public void update(User user) {
        Optional<User> existingUser = userRepository.findById(user.getUsername());
        if (existingUser.isPresent()) {
            user.setPassword(existingUser.get().getPassword());
            userRepository.saveAndFlush(user);
        }
    }

    @Override
    @Transactional
    public void updatePassword(User user) {
        Optional<User> existingUser = userRepository.findById(user.getUsername());
        if (existingUser.isPresent()) {
            userRepository.updatePassword(user.getUsername(), bCryptPasswordEncoder.encode(user.getPassword()));
        }

    }


    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }


    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }
}