package com.saviorru.jt.services;

import com.saviorru.jt.model.TM;
import com.saviorru.jt.model.TMIdentity;


import java.util.List;


public interface TMService
{
    void save(TM tm);
    void update(TM tm);
    void delete(TM tm);
    TM findById(String id);
    List<TM> getAll();
}
