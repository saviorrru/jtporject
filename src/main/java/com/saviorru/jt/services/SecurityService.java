package com.saviorru.jt.services;

import com.saviorru.jt.model.User;

public interface SecurityService {
    String findLoggedInUsername();
    User findLoggedInUser();
    void autologin(String username, String password);
    void logout();
}