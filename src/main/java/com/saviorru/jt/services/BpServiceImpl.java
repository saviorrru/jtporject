package com.saviorru.jt.services;

import com.saviorru.jt.model.BuisinessPartner;
import com.saviorru.jt.repository.BPRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BpServiceImpl implements BpService {
    private final BPRepository bpRepository;

    @Autowired
    public BpServiceImpl(BPRepository bpRepository) {
        this.bpRepository = bpRepository;
    }


    @Override
    public void save(BuisinessPartner bp) {
        this.bpRepository.saveAndFlush(bp);
    }

    @Override
    public void update(BuisinessPartner bp) {
        Optional<BuisinessPartner> existingBp = bpRepository.findById(bp.getId());
        if (existingBp.isPresent()) {
            this.bpRepository.saveAndFlush(bp);
        }
    }

    @Override
    public void delete(BuisinessPartner bp) {
        this.bpRepository.delete(bp);
    }

    @Override
    public BuisinessPartner findById(String id) {

        if (bpRepository.findById(id).isPresent()) {
            return bpRepository.findById(id).get();
        }
        return null;
    }

    @Override
    public List<BuisinessPartner> getAll() {
        return bpRepository.findAll();
    }
}
