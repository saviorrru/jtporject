package com.saviorru.jt.controllers;

import com.saviorru.jt.controllers.support.RawSdInfo;
import com.saviorru.jt.controllers.support.RawUserInfo;
import com.saviorru.jt.model.BuisinessPartner;
import com.saviorru.jt.model.SupplyDocument;
import com.saviorru.jt.services.BpService;
import com.saviorru.jt.services.DataValidator;
import com.saviorru.jt.services.SdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Controller
public class SdController {
    private SdService sdService;
    private  BpService bpService;
    private DataValidator dataValidator;


    @Autowired
    public SdController(SdService sdService, DataValidator dataValidator, BpService bpService) {
        this.sdService = sdService;
        this.dataValidator = dataValidator;
        this.bpService = bpService;
    }

    @RequestMapping(value = {"/data/sd"}, method = RequestMethod.GET)
    public String getSdList(Model model)
    {

        List<SupplyDocument> existingSds = sdService.getAll();
        List<RawSdInfo> sds = new ArrayList<>();
        for (int i=0; i<existingSds.size(); i++)
        {
            sds.add(RawSdInfo(existingSds.get(i), new RawSdInfo()));
        }
        model.addAttribute("sds", sds);
        return "input_forms/sd/sd_list";
    }

    @RequestMapping(value = {"/data/sd/create"}, method = RequestMethod.GET)
    public String getSdCreate(Model model)
    {
        model.addAttribute("sd", new RawSdInfo());
        return "input_forms/sd/sd_new";
    }

    @RequestMapping (value = {"/data/sd/create"}, method = RequestMethod.POST)
    public String createSd(@ModelAttribute("sd") RawSdInfo sd, BindingResult bindingResult, Model model)
    {
        dataValidator.validateSdId(sd, bindingResult);
        dataValidator.validateSd(sd, bindingResult);
        if (bindingResult.hasErrors()) {
            return "input_forms/sd/sd_new";
        }
        SupplyDocument newSd = new SupplyDocument();
        newSd.setRegNo(sd.getRegNo());
        newSd.setBp(bpService.findById(sd.getBp()));
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        sdf.setLenient(false);
        try {
            newSd.setStartDate(sdf.parse(sd.getStartDate()));
            newSd.setEndDate(sdf.parse(sd.getEndDate()));
        }
        catch (ParseException e)
        {}
        sdService.save(newSd);
        return "redirect:/data/sd";
    }

    @RequestMapping(value = {"/data/sd/{id}/update"}, method = RequestMethod.GET)
    public String getSdUpdate(Model model, @PathVariable("id") String id) {

        SupplyDocument existingSd = sdService.findById(id);

        model.addAttribute("sd", RawSdInfo(existingSd, new RawSdInfo()));
        return "input_forms/sd/sd_update";
    }

    @RequestMapping(value = {"/data/sd/{id}/update"}, method = RequestMethod.POST)
    public String updateSd(Model model, @ModelAttribute("sd") RawSdInfo sd, BindingResult bindingResult) {
        SupplyDocument existingSd = sdService.findById(sd.getRegNo());
        dataValidator.validateSd(sd, bindingResult);
        if (bindingResult.hasErrors()) {
            return "input_forms/sd/sd_update";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        sdf.setLenient(false);
        try {
            existingSd.setStartDate(sdf.parse(sd.getStartDate()));
            existingSd.setEndDate(sdf.parse(sd.getEndDate()));
        }
        catch (ParseException e)
        {}
        existingSd.setBp(bpService.findById(sd.getBp()));
        sdService.update(existingSd);
        return "redirect:/data/sd";
    }

    @RequestMapping(value = {"/data/sd/{id}/delete"}, method = RequestMethod.GET)
    public String getSdDelete(Model model, @PathVariable("id") String id) {
        SupplyDocument existingSd = sdService.findById(id);
        model.addAttribute("sd", existingSd);
        return "input_forms/sd/sd_delete";
    }

    @RequestMapping(value = {"/data/sd/{id}/delete"}, method = RequestMethod.POST)
    public String deleteBp(Model model, @ModelAttribute("sd") SupplyDocument sd) {
        sdService.delete(sd);
        return "redirect:/data/bp";
    }

    @Bean
    public RawSdInfo RawSdInfo(){
        return new RawSdInfo();
    }

    private RawSdInfo RawSdInfo(SupplyDocument existingSd, RawSdInfo sd)
    {

        sd.setRegNo(existingSd.getRegNo());
        if(existingSd.getStartDate()!=null)
        {
            String startDateStr = new SimpleDateFormat("dd-MM-yyyy").format(existingSd.getStartDate()).toString();
            sd.setStartDate(startDateStr);
        }
        if(existingSd.getEndDate()!=null)
        {
            String endDateStr = new SimpleDateFormat("dd-MM-yyyy").format(existingSd.getEndDate()).toString();
            sd.setStartDate(endDateStr);
        }
        if (existingSd.getBp()!=null)
        {
            sd.setBp(existingSd.getBp().getId());
        }
        return sd;

    }
}
