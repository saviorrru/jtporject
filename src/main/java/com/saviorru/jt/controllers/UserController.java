package com.saviorru.jt.controllers;

import com.saviorru.jt.controllers.support.RawUserInfo;
import com.saviorru.jt.model.Role;
import com.saviorru.jt.model.RoleName;
import com.saviorru.jt.model.User;
import com.saviorru.jt.model.UserInfo;
import com.saviorru.jt.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Email;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Controller
public class UserController {

    private final UserService userService;
    private final RoleService roleService;
    private final UserValidator userValidator;
    private final SecurityService securityService;
    private final TemplateHelper templateHelper;
    private final UserInfoService userInfoService;

    @Autowired
    public UserController(UserService userService, RoleService roleService, UserValidator userValidator, SecurityService securityService,
                          TemplateHelper templateHelper, UserInfoService userInfoService) {
        this.userService = userService;
        this.roleService = roleService;
        this.userValidator = userValidator;
        this.securityService = securityService;
        this.templateHelper = templateHelper;
        this.userInfoService = userInfoService;

    }

    @RequestMapping(value = {"/account"}, method = RequestMethod.GET)
    public String account(Model model) {
        User activeUser = securityService.findLoggedInUser();
        model.addAttribute("user", activeUser);
        RawUserInfo rawUserInfo = new RawUserInfo();
        if (activeUser.getUserInfo().getFirstName()!=null)
        {
            rawUserInfo.setFirstName(activeUser.getUserInfo().getFirstName());
        }
        if (activeUser.getUserInfo().getSecondName()!=null)
        {
            rawUserInfo.setSecondName(activeUser.getUserInfo().getSecondName());
        }
        if (activeUser.getUserInfo().getBirthDate()!=null)
        {
            String birthDateStr = new SimpleDateFormat("dd-MM-yyyy").format(activeUser.getUserInfo().getBirthDate()).toString();
            rawUserInfo.setBirthDate(birthDateStr);
        }
        model.addAttribute("userInfo", rawUserInfo);

        return "account";
    }



    @RequestMapping(value = {"/account/updateInfo"}, method = RequestMethod.POST)
    public String updateUserInfo(@ModelAttribute("user") User user, @ModelAttribute("userInfo") RawUserInfo userInfo, BindingResult bindingResult, Model model) {
        User activeUser = securityService.findLoggedInUser();
        UserInfo activeUserInfo = activeUser.getUserInfo();
        activeUserInfo.setFirstName(userInfo.getFirstName());
        activeUserInfo.setSecondName(userInfo.getSecondName());
        userValidator.validateUserInfo(userInfo, bindingResult);
        if (bindingResult.hasErrors()) {
            return "redirect:/account";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        sdf.setLenient(false);
        try {
        activeUserInfo.setBirthDate(sdf.parse(userInfo.getBirthDate()));
        }
        catch (ParseException e)
        {}

        userInfoService.save(activeUserInfo);
        return "redirect:/account";
    }

    @RequestMapping(value = {"/account/setPassword"}, method = RequestMethod.POST)
    public String setPassword( @ModelAttribute("userInfo") RawUserInfo userInfo, @ModelAttribute("user") User user, BindingResult bindingResult, Model model) {
        userValidator.validatePassword(user, bindingResult);
        if (bindingResult.hasErrors()) {
            return "account";
        }
        userService.updatePassword(user);
        return "redirect:/account";
    }

    @Bean
    public RawUserInfo RawUserInfo(){
        return new RawUserInfo();
    }

    @RequestMapping(value = {"/users"}, method = RequestMethod.GET)
    public String listOfUsers(Model model) {
        List<User> users = userService.getAll();
        model.addAttribute("users", users);
        return "users/users_list";
    }

    @RequestMapping(value = {"/users/{username}/changeRole"}, method = RequestMethod.GET)
    public String getChangeRoleForm(Model model, @PathVariable("username") String username) {
        User user = userService.findByUsername(username);
        boolean isAdmin = templateHelper.isAdmin(user);

        model.addAttribute("username", username);
        model.addAttribute("admin", isAdmin);

        return "users/change_role";
    }

    @RequestMapping(value = {"/users/{username}/changeRole"}, method = RequestMethod.POST)
    public String changeRole(Model model, @PathVariable("username") String username, @RequestParam(value = "admin", defaultValue = "false") boolean admin) {
        User user = userService.findByUsername(username);
        Role adminRole = roleService.findRoleByName(RoleName.ADMIN.name);

        boolean userIsAdmin = user.getRoles().contains(adminRole);

        if (userIsAdmin != admin) {
            if (admin) {
                roleService.addRoleToUser(adminRole, user);
            } else {
                roleService.removeRoleFromUser(adminRole, user);
            }
        }

        return "redirect:/users";
    }

    @RequestMapping(value = {"/users/{username}/delete"}, method = RequestMethod.GET)
    public String deleteUserConfirmation(Model model, @PathVariable("username") String username) {
        model.addAttribute("username", username);
        return "users/user_delete";
    }

    @RequestMapping(value = {"/users/{username}/delete"}, method = RequestMethod.POST)
    public String deleteUser(Model model, @PathVariable("username") String username) {
        User user = userService.findByUsername(username);
        if (user != null) {
            userService.delete(user);
        }
        return "redirect:/users";
    }
}
