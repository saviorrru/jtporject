package com.saviorru.jt.controllers.support;


import org.springframework.beans.factory.annotation.Autowired;

public class RawUserInfo {
    private String firstName;
    private String secondName;
    private String lastName;
    private String birthDate;
    private String email;

    public RawUserInfo() {
        this.firstName = "";
        this.secondName = "";
        this.lastName = "";
        this.birthDate = "";
        this.email = "";
    }



    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}