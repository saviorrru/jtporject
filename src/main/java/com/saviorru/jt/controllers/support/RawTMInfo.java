package com.saviorru.jt.controllers.support;


public class RawTMInfo {
    private String objectId;
    private String objectName;
    private String averageV;
    private String cost;
    private String kn;
    private String power;
    private String f;
    private String askue;
    private String model;
    private String no;

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    private String document;


    public RawTMInfo() {
        objectId ="";
        objectName="";
        averageV="";
        cost="";
        kn="";
        power="";
        f="";
        askue="";
        model="";
        no="";
        document="";
    }


    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getAverageV() {
        return averageV;
    }

    public void setAverageV(String averageV) {
        this.averageV = averageV;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getKn() {
        return kn;
    }

    public void setKn(String kn) {
        this.kn = kn;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getF() {
        return f;
    }

    public void setF(String f) {
        this.f = f;
    }

    public String getAskue() {
        return askue;
    }

    public void setAskue(String askue) {
        this.askue = askue;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }
}
