package com.saviorru.jt.controllers.support;


import com.saviorru.jt.model.SupplyDocument;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class RawSdInfo {
    private String regNo;
    private String startDate;
    private String endDate;
    private String bp;


    public RawSdInfo() {
        this.regNo="";
        this.startDate="";
        this.endDate="";
        this.bp="";
    }


    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getBp() {
        return bp;
    }

    public void setBp(String bp) {
        this.bp = bp;
    }
}
