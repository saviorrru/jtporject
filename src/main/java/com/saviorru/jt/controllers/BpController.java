package com.saviorru.jt.controllers;


import com.saviorru.jt.model.BuisinessPartner;
import com.saviorru.jt.services.BpService;
import com.saviorru.jt.services.DataValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class BpController {
    private BpService bpService;
    private DataValidator dataValidator;

    @Autowired
    public BpController(BpService bpService, DataValidator dataValidator) {
        this.bpService = bpService;
        this.dataValidator = dataValidator;
    }

    @RequestMapping(value = {"/data/bp"}, method = RequestMethod.GET)
    public String getBpList(Model model)
    {
        List<BuisinessPartner> bps = bpService.getAll();
        model.addAttribute("bps", bps);
        return "input_forms/bp/bp_list";
    }

    @RequestMapping(value = {"/data/bp/create"}, method = RequestMethod.GET)
    public String getBpCreate(Model model)
    {
        model.addAttribute("bp", new BuisinessPartner());
        return "input_forms/bp/bp_new";
    }

    @RequestMapping (value = {"/data/bp/create"}, method = RequestMethod.POST)
    public String createBp(@ModelAttribute("bp") BuisinessPartner bp, BindingResult bindingResult, Model model)
    {
        dataValidator.validateBp(bp, bindingResult);
        if (bindingResult.hasErrors()) {
            return "input_forms/bp/bp_new";
        }
        bpService.save(bp);
        return "redirect:/data/bp";
    }

    @RequestMapping(value = {"/data/bp/{id}/update"}, method = RequestMethod.GET)
    public String getBpUpdate(Model model, @PathVariable("id") String id) {
        BuisinessPartner existingBp = bpService.findById(id);
        model.addAttribute("bp", existingBp);
        return "input_forms/bp/bp_update";
    }

    @RequestMapping(value = {"/data/bp/{id}/update"}, method = RequestMethod.POST)
    public String updateBp(Model model, @ModelAttribute("bp") BuisinessPartner bp) {
        bpService.update(bp);
        return "redirect:/data/bp";
    }

    @RequestMapping(value = {"/data/bp/{id}/delete"}, method = RequestMethod.GET)
    public String getBpDelete(Model model, @PathVariable("id") String id) {
        BuisinessPartner existingBp = bpService.findById(id);
        model.addAttribute("bp", existingBp);
        return "input_forms/bp/bp_delete";
    }

    @RequestMapping(value = {"/data/bp/{id}/delete"}, method = RequestMethod.POST)
    public String deleteBp(Model model, @ModelAttribute("bp") BuisinessPartner bp) {
        bpService.delete(bp);
        return "redirect:/data/bp";
    }
}
