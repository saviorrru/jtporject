package com.saviorru.jt.controllers;


import com.saviorru.jt.controllers.support.RawUserInfo;
import com.saviorru.jt.model.User;
import com.saviorru.jt.services.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collections;

@Controller
public class IndexController {

    private final SecurityService securityService;

    @Autowired
    public IndexController(SecurityService securityService) {
        this.securityService = securityService;
    }

    @RequestMapping(value={"/"})
    public String getIndexPage(Model model) {
        if (!(model.containsAttribute("loginForm")))
        {
            model.addAttribute("loginForm", new User());
        }
        return "index";
    }
}