package com.saviorru.jt.controllers;


import com.saviorru.jt.controllers.support.RawSdInfo;
import com.saviorru.jt.controllers.support.RawTMInfo;
import com.saviorru.jt.model.BuisinessPartner;
import com.saviorru.jt.model.TM;
import com.saviorru.jt.services.BpService;
import com.saviorru.jt.services.DataValidator;
import com.saviorru.jt.services.SdService;
import com.saviorru.jt.services.TMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

@Controller
public class TMController {
    private SdService sdService;
    private TMService tmService;
    private DataValidator dataValidator;

    @Autowired
    public TMController(SdService sdService, TMService tmService, DataValidator dataValidator) {
        this.sdService = sdService;
        this.tmService = tmService;
        this.dataValidator = dataValidator;
    }

    private RawTMInfo RawTMInfo(TM tm)
    {
        RawTMInfo newTMInfo = new RawTMInfo();
        if (tm.getObjectId()!=null)
        {newTMInfo.setObjectId(tm.getObjectId());}
        if (tm.getObjectName()!=null)
        {newTMInfo.setObjectName(tm.getObjectName());}
        if (tm.getAverageV()!=null)
        {newTMInfo.setAverageV(tm.getAverageV().toString());}
        if (tm.getCost()!=null)
        {newTMInfo.setCost(tm.getCost().toString());}
        if (tm.getKn()!=null)
        {newTMInfo.setKn(tm.getKn().toString());}
        if (tm.getPower()!=null)
        {newTMInfo.setPower(tm.getPower().toString());}
        if (tm.getF()!=null)
        {newTMInfo.setF(tm.getF().toString());}
        if (tm.getAskue()!=null)
        {newTMInfo.setAskue(tm.getAskue().toString());}
        if (tm.getModel()!=null)
        {newTMInfo.setModel(tm.getModel().toString());}
        if (tm.getNo()!=null)
        {newTMInfo.setNo(tm.getNo().toString());}
        if (tm.getDocument()!=null)
        {newTMInfo.setDocument(tm.getDocument().getRegNo());}
        return  newTMInfo;
    }

    private TM TM(RawTMInfo rawTMInfo)
    {
        TM newTM = new TM();
        newTM.setObjectId(rawTMInfo.getObjectId());
        newTM.setObjectName(rawTMInfo.getObjectName());
        if (rawTMInfo.getAverageV() !="")
        {newTM.setAverageV(Float.parseFloat(rawTMInfo.getAverageV()));}
        if (rawTMInfo.getCost() !="")
        {newTM.setCost(Float.parseFloat(rawTMInfo.getCost()));}
        newTM.setKn(rawTMInfo.getKn());
        if (rawTMInfo.getPower() !="")
        {newTM.setPower(Float.parseFloat(rawTMInfo.getPower()));}
        newTM.setF(rawTMInfo.getF());
        newTM.setAskue(rawTMInfo.getAskue());
        newTM.setModel(rawTMInfo.getModel());
        newTM.setNo(rawTMInfo.getNo());
        newTM.setDocument(sdService.findById(rawTMInfo.getDocument()));
        return  newTM;
    }


    @RequestMapping(value = {"/data/tm"}, method = RequestMethod.GET)
    public String getTMList(Model model)
    {
        List<TM> existingTMs = tmService.getAll();
        List<RawTMInfo> tms = new ArrayList<>();
        for (int i=0; i<existingTMs.size(); i++)
        {
            tms.add(RawTMInfo(existingTMs.get(i)));
        }
        model.addAttribute("tms", tms);
        return "input_forms/tm/tm_list";
    }

    @RequestMapping(value = {"/data/tm/create"}, method = RequestMethod.GET)
    public String getTMCreate(Model model)
    {
        model.addAttribute("tm", new RawTMInfo());
        return "input_forms/tm/tm_new";
    }

    @RequestMapping (value = {"/data/tm/create"}, method = RequestMethod.POST)
    public String createTM(@ModelAttribute("tm") RawTMInfo tm, BindingResult bindingResult, Model model)
    {
        dataValidator.validateTM(tm, bindingResult);
        dataValidator.validateTMId(tm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "input_forms/tm/tm_new";
        }
        TM newTM = TM(tm);
        tmService.save(newTM);
        return "redirect:/data/tm";
    }

    @RequestMapping(value = {"/data/tm/{id}/update"}, method = RequestMethod.GET)
    public String getTMUpdate(Model model, @PathVariable("id") String id) {
        TM existingTM = tmService.findById(id);
        RawTMInfo tm = RawTMInfo(existingTM);
        model.addAttribute("tm", tm);
        return "input_forms/tm/tm_update";
    }

    @RequestMapping(value = {"/data/tm/{id}/update"}, method = RequestMethod.POST)
    public String updateTM(Model model, @ModelAttribute("tm") RawTMInfo tm, BindingResult bindingResult) {
        dataValidator.validateTM(tm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "input_forms/tm/tm_update";
        }
        TM newTM = TM(tm);
        tmService.update(newTM);
        return "redirect:/data/tm";
    }

    @RequestMapping(value = {"/data/tm/{id}/delete"}, method = RequestMethod.GET)
    public String getTMDelete(Model model, @PathVariable("id") String id) {
        TM existingTM = tmService.findById(id);
        model.addAttribute("tm", existingTM);
        return "input_forms/tm/tm_delete";
    }

    @RequestMapping(value = {"/data/tm/{id}/delete"}, method = RequestMethod.POST)
    public String deleteTM(Model model, @ModelAttribute("bp") TM tm) {

        tmService.delete(tm);
        return "redirect:/data/tm";
    }




}
