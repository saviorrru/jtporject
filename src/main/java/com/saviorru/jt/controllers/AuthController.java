package com.saviorru.jt.controllers;

import com.saviorru.jt.model.User;
import com.saviorru.jt.services.SecurityService;
import com.saviorru.jt.services.UserService;
import com.saviorru.jt.services.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;

@Controller
public class AuthController {
    private final UserService userService;
    private final SecurityService securityService;
    private final UserValidator userValidator;

    @Autowired
    public AuthController(UserService userService, SecurityService securityService, UserValidator userValidator) {
        this.userService = userService;
        this.securityService = securityService;
        this.userValidator = userValidator;
    }


    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String getSignUpForm(Model model) {
        model.addAttribute("userForm", new User());

        return "signup";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String signUp(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model) {
        if (!(model.containsAttribute("loginForm")))
        {
            model.addAttribute("loginForm", new User());
        }
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "/signup";
        }
        userService.save(userForm);


        return "index";
    }


    @RequestMapping(value = "/login",  method = RequestMethod.GET)
    public String getLogin( Model model, String error, String logout) {
        if (!(model.containsAttribute("loginForm")))
        {
            model.addAttribute("loginForm", new User());
        }
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");
        return "index";
    }


}

