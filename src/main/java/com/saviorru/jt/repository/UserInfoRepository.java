package com.saviorru.jt.repository;

import com.saviorru.jt.model.User;
import com.saviorru.jt.model.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserInfoRepository extends JpaRepository<UserInfo, Long> {
    UserInfo findByUser(User user);

}
