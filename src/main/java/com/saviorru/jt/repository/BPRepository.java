package com.saviorru.jt.repository;

import com.saviorru.jt.model.BuisinessPartner;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BPRepository extends JpaRepository<BuisinessPartner, String> {


}
