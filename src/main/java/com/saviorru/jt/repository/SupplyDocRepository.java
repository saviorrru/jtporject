package com.saviorru.jt.repository;

import com.saviorru.jt.model.SupplyDocument;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupplyDocRepository extends JpaRepository<SupplyDocument, String> {

}
