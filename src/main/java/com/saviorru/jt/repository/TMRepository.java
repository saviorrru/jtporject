package com.saviorru.jt.repository;

import com.saviorru.jt.model.TM;
import com.saviorru.jt.model.TMIdentity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TMRepository extends JpaRepository <TM, String> {
}
