package com.saviorru.jt.repository;

import com.saviorru.jt.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, String> {

    User findByUsername(String username);
    @Modifying
    @Query("update User t set t.password = :password where t.username = :username")
    void updatePassword(@Param("username") String username, @Param("password") String password);
}
